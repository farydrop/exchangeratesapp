package com.example.retrofitmoney.model.cash

data class NalichkaItem(
    val base_ccy: String,
    val buy: String,
    val ccy: String,
    val sale: String
)