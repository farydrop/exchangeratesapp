package com.example.retrofitmoney.model

data class Result(
    val name: String,
    val url: String
)