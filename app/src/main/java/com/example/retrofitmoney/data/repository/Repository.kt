package com.example.retrofitmoney.data.repository

import com.example.retrofitmoney.data.api.RetrofitInstance
import com.example.retrofitmoney.model.cash.Nalichka
import com.example.retrofitmoney.model.cash.NalichkaItem
import com.example.retrofitmoney.model.nocash.Beznal
import com.example.retrofitmoney.model.nocash.BeznalItem
import retrofit2.Response

class Repository {

    suspend fun getCashMoney(): Response<Nalichka>{
        return RetrofitInstance.api.getCash()
    }

    suspend fun getNocashMoney(): Response<Beznal>{
        return RetrofitInstance.api.getNoCash()
    }

}