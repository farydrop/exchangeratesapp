package com.example.retrofitmoney.data.api

import com.example.retrofitmoney.model.cash.Nalichka
import com.example.retrofitmoney.model.cash.NalichkaItem
import com.example.retrofitmoney.model.nocash.Beznal
import com.example.retrofitmoney.model.nocash.BeznalItem
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("p24api/pubinfo?json&exchange&coursid=5")
    suspend fun getCash(): Response<Nalichka>

    @GET("p24api/pubinfo?exchange&json&coursid=11")
    suspend fun getNoCash():Response<Beznal>

}