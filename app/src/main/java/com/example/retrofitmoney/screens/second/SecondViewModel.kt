package com.example.retrofitmoney.screens.second

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.retrofitmoney.data.repository.Repository
import com.example.retrofitmoney.model.cash.Nalichka
import com.example.retrofitmoney.model.nocash.Beznal
import kotlinx.coroutines.launch
import retrofit2.Response

class SecondViewModel:ViewModel() {

    var repo = Repository()
    val myMoneyList: MutableLiveData<Response<Beznal>> = MutableLiveData()

    fun getNocashMoney(){
        viewModelScope.launch {
            myMoneyList.value = repo.getNocashMoney()
        }
    }
}